# CatVodOpen

#### 介绍
CatVod点播源，资源收集网络


#### 使用说明

1.  把js整个目录拷出到桌面
2.  打开终端，把配置文件整个拷贝到内置目录下

```
cd /Applications/猫影视.app/Wrapper/Runner.app/Frameworks/App.framework/flutter_assets/asset/
 
# 然后备份默认的js目录
mv js{,_bak}
 
# 注意修改 xxx 为你的实际用户名
cp -r /Users/xxx/Desktop/js ./
```

3.  配置填写assets://js/config_open.json配置好之后直接回车

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
